-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-09-2015 a las 20:00:13
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pgp_db_proyecto_gestion_de_proyectos_database`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla ` tb_acc_estado`
--

CREATE TABLE IF NOT EXISTS ` tb_acc_estado` (
` tb_acc_estado_id_estado` int(11) NOT NULL,
  `tb_acc_estado_descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos del estado  que se registraran';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_acc_perfiles`
--

CREATE TABLE IF NOT EXISTS `tb_acc_perfiles` (
`tb_acc_perfiles_id_perfiles` int(11) NOT NULL,
  `tb_acc_perfiles_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los perfiles  que se registraran';

--
-- Volcado de datos para la tabla `tb_acc_perfiles`
--

INSERT INTO `tb_acc_perfiles` (`tb_acc_perfiles_id_perfiles`, `tb_acc_perfiles_nombre`) VALUES
(1, 'Administrador'),
(2, 'Instructor'),
(3, 'Aprendiz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_acc_programa_de_formacion`
--

CREATE TABLE IF NOT EXISTS `tb_acc_programa_de_formacion` (
  `tb_acc_programa_de_formacion_id_programa_de_formaciion` int(11) NOT NULL,
  `tb_acc_programa_de_formacion_nombre` varchar(100) NOT NULL,
  `tb_acc_programa_de_formacion_descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los programas de formacion  que se registraran';

--
-- Volcado de datos para la tabla `tb_acc_programa_de_formacion`
--

INSERT INTO `tb_acc_programa_de_formacion` (`tb_acc_programa_de_formacion_id_programa_de_formaciion`, `tb_acc_programa_de_formacion_nombre`, `tb_acc_programa_de_formacion_descripcion`) VALUES
(1, 'adsi', 'software');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_acc_roles`
--

CREATE TABLE IF NOT EXISTS `tb_acc_roles` (
`tb_acc_roles_id_roles` int(11) NOT NULL,
  `tb_acc_roles_nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los roles  que se registraran';

--
-- Volcado de datos para la tabla `tb_acc_roles`
--

INSERT INTO `tb_acc_roles` (`tb_acc_roles_id_roles`, `tb_acc_roles_nombre`) VALUES
(1, 'desarrollador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_acc_usuario_grupo`
--

CREATE TABLE IF NOT EXISTS `tb_acc_usuario_grupo` (
`tb_acc_usuario_grupo_id_usuario_grupo` int(11) NOT NULL,
  `tb_pro_usuario_id_usuario` int(11) NOT NULL,
  `tb_pro_grupo_trabajo_id_grupo_trabajo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='esta tabla es una coneccion de la tabla usuario y grupo';

--
-- Volcado de datos para la tabla `tb_acc_usuario_grupo`
--

INSERT INTO `tb_acc_usuario_grupo` (`tb_acc_usuario_grupo_id_usuario_grupo`, `tb_pro_usuario_id_usuario`, `tb_pro_grupo_trabajo_id_grupo_trabajo`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_acc_usuario_rol`
--

CREATE TABLE IF NOT EXISTS `tb_acc_usuario_rol` (
`tb_acc_usuario_rol_id_usuario_rol` int(11) NOT NULL,
  `tb_pro_usuario_id_usuario` int(11) NOT NULL,
  `tb_acc_roles_id_roles` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='tabla que relacionara las tablas usuario y roles';

--
-- Volcado de datos para la tabla `tb_acc_usuario_rol`
--

INSERT INTO `tb_acc_usuario_rol` (`tb_acc_usuario_rol_id_usuario_rol`, `tb_pro_usuario_id_usuario`, `tb_acc_roles_id_roles`) VALUES
(2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_actividades`
--

CREATE TABLE IF NOT EXISTS `tb_pro_actividades` (
`tb_pro_actividades_id_actividades` int(11) NOT NULL,
  `tb_pro_actividades_descripcion` varchar(100) NOT NULL,
  `tb_pro_actividades_fecha_inicio` date NOT NULL,
  `tb_pro_actividades_fecha_fin` date NOT NULL,
  `tb_pro_fases_id_fase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de las actividades  que se registraran';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_fases`
--

CREATE TABLE IF NOT EXISTS `tb_pro_fases` (
`tb_pro_fases_id_fase` int(11) NOT NULL,
  `tb_pro_fases_descripcion` varchar(100) NOT NULL,
  `tb_pro_fases_fecha_inicio` date NOT NULL,
  `tb_pro_fases_fecha_fin` date NOT NULL,
  `tb_acc_estado_id_estado` int(11) NOT NULL,
  `tb_pro_proyectos_id_proyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabala que almacenara los datos que se registraran en las fases del proyecto';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_grupo_trabajo`
--

CREATE TABLE IF NOT EXISTS `tb_pro_grupo_trabajo` (
`tb_pro_grupo_trabajo_id_grupo_trabajo` int(11) NOT NULL,
  `tb_pro_grupo_trabajo_nombre` varchar(50) NOT NULL,
  `tb_pro_grupo_trabajo_descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los grupos de trabajo  que se registraran';

--
-- Volcado de datos para la tabla `tb_pro_grupo_trabajo`
--

INSERT INTO `tb_pro_grupo_trabajo` (`tb_pro_grupo_trabajo_id_grupo_trabajo`, `tb_pro_grupo_trabajo_nombre`, `tb_pro_grupo_trabajo_descripcion`) VALUES
(1, 'primer grupo', 'este es una prueba de un grupo '),
(2, 'calidad', 'hacer calidad al software');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_producto`
--

CREATE TABLE IF NOT EXISTS `tb_pro_producto` (
` tb_pro_producto_id_producto` int(11) NOT NULL,
  `tb_pro_producto_descripcion` varchar(100) NOT NULL,
  `tb_pro_producto_fecha_entrega` date NOT NULL,
  `tb_pro_fases_id_fase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los productos  que se registraran';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_proyectos`
--

CREATE TABLE IF NOT EXISTS `tb_pro_proyectos` (
`tb_pro_proyectos_id_proyecto` int(11) NOT NULL,
  `tb_pro_proyectos_nombreproyecto` varchar(200) NOT NULL,
  `tb_pro_grupo_trabajo_id_grupo_trabajo` int(11) NOT NULL,
  `tb_pro_proyectos_fecha` varchar(30) NOT NULL,
  `tb_pro_proyectos_descripcion` varchar(100) NOT NULL,
  `tb_pro_proyectos_resumen` varchar(100) NOT NULL,
  `tb_pro_proyectos_area` varchar(50) NOT NULL,
  `tb_pro_proyectos_objetivo` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los proyectos a que se registraran';

--
-- Volcado de datos para la tabla `tb_pro_proyectos`
--

INSERT INTO `tb_pro_proyectos` (`tb_pro_proyectos_id_proyecto`, `tb_pro_proyectos_nombreproyecto`, `tb_pro_grupo_trabajo_id_grupo_trabajo`, `tb_pro_proyectos_fecha`, `tb_pro_proyectos_descripcion`, `tb_pro_proyectos_resumen`, `tb_pro_proyectos_area`, `tb_pro_proyectos_objetivo`) VALUES
(1, '', 1, '2015-09-02', 'proyecto', 'este es prueba de primer proyecto', 'calidad', 'hacer un proyecto software'),
(2, 'segundo proyecto', 1, '2015-09-01', 'segundo proyecto', 'prueba proyecto', 'software', 'prueba proyecto'),
(3, 'Gestion calidad', 1, '2015-09-12 05:00:00Z', 'calidad', 'permite llevar una lista adecuada de los procedimientos de calidad', 'calidad', 'calidad'),
(4, 'software', 1, '2015-09-28 05:00:00Z', 'algo', 'test', 'test prueba', 'algomas'),
(5, 'scrum', 1, '2015-09-13 05:00:00Z', 'algo de calidad', 'software', 'calidad', 'algo mas'),
(6, 'psp', 1, '2015-09-13 05:00:00Z', 'software', 'proceso personal de software', 'sftware', 'procesos '),
(7, 'psp2', 1, '2015-09-12 05:00:00Z', 'software', 'algo', 'software', 'software'),
(11, 'ka', 1, '2015-09-07 05:00:00Z', 'h', 'b', ' h', 'u'),
(12, 'ew', 1, '2015-09-09 05:00:00Z', 'ee', 'ef', 'ff', 'ff'),
(13, 'er', 1, '2015-09-28 05:00:00Z', 'rf', 'frfe', 'rfer', 'ferf'),
(14, 'sds', 1, '2015-09-29 05:00:00Z', 'fesd', 'sdfsdfs', 'sdfsdf', 'dfsd'),
(15, 'qweq', 1, '2015-09-28 05:00:00Z', 'wqeq', 'we', 'qweqw', 'weq');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pro_usuario`
--

CREATE TABLE IF NOT EXISTS `tb_pro_usuario` (
`tb_pro_usuario_id_usuario` int(11) NOT NULL,
  `tb_acc_perfiles_id_perfiles` int(11) NOT NULL,
  `tb_acc_programa_de_formacion_id_programa_de_formacion` int(11) NOT NULL,
  `tb_pro_usuario_login` varchar(50) NOT NULL,
  `tb_pro_usuario_pass` varchar(700) NOT NULL,
  `tb_pro_usuario_identificacion` int(15) NOT NULL,
  `tb_pro_usuario_nombres` varchar(50) NOT NULL,
  `tb_pro_usuario_apellidos` varchar(50) NOT NULL,
  `tb_pro_usuario_lider` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='tabla que contendra los datos de los usuarios a que se registraran';

--
-- Volcado de datos para la tabla `tb_pro_usuario`
--

INSERT INTO `tb_pro_usuario` (`tb_pro_usuario_id_usuario`, `tb_acc_perfiles_id_perfiles`, `tb_acc_programa_de_formacion_id_programa_de_formacion`, `tb_pro_usuario_login`, `tb_pro_usuario_pass`, `tb_pro_usuario_identificacion`, `tb_pro_usuario_nombres`, `tb_pro_usuario_apellidos`, `tb_pro_usuario_lider`) VALUES
(1, 1, 1, 'sena', 'america', 1234, 'yovani', 'burbano', 'Lider'),
(2, 2, 1, 'ramones', 'ramones', 12345, 'ramon', 'valdez', 'Lider'),
(3, 2, 1, 'flor2', 'real', 2344, 'florinda', 'meza', 'Aprendiz');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla ` tb_acc_estado`
--
ALTER TABLE ` tb_acc_estado`
 ADD PRIMARY KEY (` tb_acc_estado_id_estado`);

--
-- Indices de la tabla `tb_acc_perfiles`
--
ALTER TABLE `tb_acc_perfiles`
 ADD PRIMARY KEY (`tb_acc_perfiles_id_perfiles`);

--
-- Indices de la tabla `tb_acc_programa_de_formacion`
--
ALTER TABLE `tb_acc_programa_de_formacion`
 ADD PRIMARY KEY (`tb_acc_programa_de_formacion_id_programa_de_formaciion`), ADD KEY `tb_acc_programa_de_formacion_id_programa_de_formaciion` (`tb_acc_programa_de_formacion_id_programa_de_formaciion`);

--
-- Indices de la tabla `tb_acc_roles`
--
ALTER TABLE `tb_acc_roles`
 ADD PRIMARY KEY (`tb_acc_roles_id_roles`);

--
-- Indices de la tabla `tb_acc_usuario_grupo`
--
ALTER TABLE `tb_acc_usuario_grupo`
 ADD PRIMARY KEY (`tb_acc_usuario_grupo_id_usuario_grupo`), ADD KEY `tb_pro_usuario_id_usuario` (`tb_pro_usuario_id_usuario`), ADD KEY `tb_pro_grupo_trabajo_id_grupo_trabajo` (`tb_pro_grupo_trabajo_id_grupo_trabajo`), ADD KEY `tb_pro_usuario_id_usuario_2` (`tb_pro_usuario_id_usuario`);

--
-- Indices de la tabla `tb_acc_usuario_rol`
--
ALTER TABLE `tb_acc_usuario_rol`
 ADD PRIMARY KEY (`tb_acc_usuario_rol_id_usuario_rol`), ADD KEY `tb_pro_usuario_id_usuario` (`tb_pro_usuario_id_usuario`), ADD KEY `tb_pro_usuario_id_usuario_2` (`tb_pro_usuario_id_usuario`), ADD KEY `tb_acc_roles_id_roles` (`tb_acc_roles_id_roles`), ADD KEY `tb_pro_usuario_id_usuario_3` (`tb_pro_usuario_id_usuario`);

--
-- Indices de la tabla `tb_pro_actividades`
--
ALTER TABLE `tb_pro_actividades`
 ADD PRIMARY KEY (`tb_pro_actividades_id_actividades`), ADD KEY `tb_pro_fases_id_fase` (`tb_pro_fases_id_fase`);

--
-- Indices de la tabla `tb_pro_fases`
--
ALTER TABLE `tb_pro_fases`
 ADD PRIMARY KEY (`tb_pro_fases_id_fase`), ADD KEY `tb_pro_fases_id_fase` (`tb_pro_fases_id_fase`), ADD KEY `tb_acc_estado_id_estado` (`tb_acc_estado_id_estado`), ADD KEY `tb_pro_proyectos_id_proyecto` (`tb_pro_proyectos_id_proyecto`), ADD KEY `tb_pro_proyectos_id_proyecto_2` (`tb_pro_proyectos_id_proyecto`), ADD KEY `tb_acc_estado_id_estado_2` (`tb_acc_estado_id_estado`), ADD KEY `tb_pro_fases_id_fase_2` (`tb_pro_fases_id_fase`);

--
-- Indices de la tabla `tb_pro_grupo_trabajo`
--
ALTER TABLE `tb_pro_grupo_trabajo`
 ADD PRIMARY KEY (`tb_pro_grupo_trabajo_id_grupo_trabajo`);

--
-- Indices de la tabla `tb_pro_producto`
--
ALTER TABLE `tb_pro_producto`
 ADD PRIMARY KEY (` tb_pro_producto_id_producto`), ADD KEY `tb_pro_fases_id_fase` (`tb_pro_fases_id_fase`);

--
-- Indices de la tabla `tb_pro_proyectos`
--
ALTER TABLE `tb_pro_proyectos`
 ADD PRIMARY KEY (`tb_pro_proyectos_id_proyecto`), ADD KEY `tb_pro_grupo_trabajo_id_grupo_trabajo` (`tb_pro_grupo_trabajo_id_grupo_trabajo`);

--
-- Indices de la tabla `tb_pro_usuario`
--
ALTER TABLE `tb_pro_usuario`
 ADD PRIMARY KEY (`tb_pro_usuario_id_usuario`), ADD KEY `tb_acc_perfiles_id_perfiles` (`tb_acc_perfiles_id_perfiles`), ADD KEY `tb_acc_programa_de_formacion_id_programa_de_formacion` (`tb_acc_programa_de_formacion_id_programa_de_formacion`), ADD KEY `tb_acc_programa_de_formacion_i_2` (`tb_acc_programa_de_formacion_id_programa_de_formacion`), ADD KEY `tb_pro_usuario_id_usuario` (`tb_pro_usuario_id_usuario`), ADD KEY `tb_acc_programa_de_formacion_i_3` (`tb_acc_programa_de_formacion_id_programa_de_formacion`), ADD KEY `tb_acc_programa_de_formacion_i_4` (`tb_acc_programa_de_formacion_id_programa_de_formacion`), ADD KEY `tb_pro_usuario_id_usuario_2` (`tb_pro_usuario_id_usuario`), ADD KEY `tb_pro_usuario_id_usuario_3` (`tb_pro_usuario_id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla ` tb_acc_estado`
--
ALTER TABLE ` tb_acc_estado`
MODIFY ` tb_acc_estado_id_estado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tb_acc_perfiles`
--
ALTER TABLE `tb_acc_perfiles`
MODIFY `tb_acc_perfiles_id_perfiles` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tb_acc_roles`
--
ALTER TABLE `tb_acc_roles`
MODIFY `tb_acc_roles_id_roles` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_acc_usuario_grupo`
--
ALTER TABLE `tb_acc_usuario_grupo`
MODIFY `tb_acc_usuario_grupo_id_usuario_grupo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_acc_usuario_rol`
--
ALTER TABLE `tb_acc_usuario_rol`
MODIFY `tb_acc_usuario_rol_id_usuario_rol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tb_pro_actividades`
--
ALTER TABLE `tb_pro_actividades`
MODIFY `tb_pro_actividades_id_actividades` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tb_pro_fases`
--
ALTER TABLE `tb_pro_fases`
MODIFY `tb_pro_fases_id_fase` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tb_pro_grupo_trabajo`
--
ALTER TABLE `tb_pro_grupo_trabajo`
MODIFY `tb_pro_grupo_trabajo_id_grupo_trabajo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tb_pro_producto`
--
ALTER TABLE `tb_pro_producto`
MODIFY ` tb_pro_producto_id_producto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tb_pro_proyectos`
--
ALTER TABLE `tb_pro_proyectos`
MODIFY `tb_pro_proyectos_id_proyecto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `tb_pro_usuario`
--
ALTER TABLE `tb_pro_usuario`
MODIFY `tb_pro_usuario_id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tb_acc_usuario_grupo`
--
ALTER TABLE `tb_acc_usuario_grupo`
ADD CONSTRAINT `tb_acc_usuario_grupo_ibfk_1` FOREIGN KEY (`tb_pro_grupo_trabajo_id_grupo_trabajo`) REFERENCES `tb_pro_grupo_trabajo` (`tb_pro_grupo_trabajo_id_grupo_trabajo`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tb_acc_usuario_grupo_ibfk_2` FOREIGN KEY (`tb_pro_usuario_id_usuario`) REFERENCES `tb_pro_usuario` (`tb_pro_usuario_id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_acc_usuario_rol`
--
ALTER TABLE `tb_acc_usuario_rol`
ADD CONSTRAINT `tb_acc_usuario_rol_ibfk_2` FOREIGN KEY (`tb_acc_roles_id_roles`) REFERENCES `tb_acc_roles` (`tb_acc_roles_id_roles`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tb_acc_usuario_rol_ibfk_3` FOREIGN KEY (`tb_pro_usuario_id_usuario`) REFERENCES `tb_pro_usuario` (`tb_pro_usuario_id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_pro_actividades`
--
ALTER TABLE `tb_pro_actividades`
ADD CONSTRAINT `tb_pro_actividades_ibfk_1` FOREIGN KEY (`tb_pro_fases_id_fase`) REFERENCES `tb_pro_fases` (`tb_pro_fases_id_fase`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_pro_fases`
--
ALTER TABLE `tb_pro_fases`
ADD CONSTRAINT `tb_pro_fases_ibfk_1` FOREIGN KEY (`tb_acc_estado_id_estado`) REFERENCES ` tb_acc_estado` (` tb_acc_estado_id_estado`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tb_pro_fases_ibfk_2` FOREIGN KEY (`tb_pro_proyectos_id_proyecto`) REFERENCES `tb_pro_fases` (`tb_pro_proyectos_id_proyecto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_pro_producto`
--
ALTER TABLE `tb_pro_producto`
ADD CONSTRAINT `tb_pro_producto_ibfk_1` FOREIGN KEY (`tb_pro_fases_id_fase`) REFERENCES `tb_pro_fases` (`tb_pro_fases_id_fase`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_pro_proyectos`
--
ALTER TABLE `tb_pro_proyectos`
ADD CONSTRAINT `tb_pro_proyectos_ibfk_1` FOREIGN KEY (`tb_pro_grupo_trabajo_id_grupo_trabajo`) REFERENCES `tb_pro_grupo_trabajo` (`tb_pro_grupo_trabajo_id_grupo_trabajo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tb_pro_usuario`
--
ALTER TABLE `tb_pro_usuario`
ADD CONSTRAINT `tb_pro_usuario_ibfk_2` FOREIGN KEY (`tb_acc_perfiles_id_perfiles`) REFERENCES `tb_acc_perfiles` (`tb_acc_perfiles_id_perfiles`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tb_pro_usuario_ibfk_3` FOREIGN KEY (`tb_acc_programa_de_formacion_id_programa_de_formacion`) REFERENCES `tb_acc_programa_de_formacion` (`tb_acc_programa_de_formacion_id_programa_de_formaciion`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
