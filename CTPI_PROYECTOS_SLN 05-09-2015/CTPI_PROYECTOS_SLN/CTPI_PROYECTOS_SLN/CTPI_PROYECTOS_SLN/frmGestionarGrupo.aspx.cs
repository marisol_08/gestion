﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using CTPI_PROYECTOS_SLN.Logica;
using System.Web.UI.WebControls;

namespace CTPI_PROYECTOS_SLN
{
    public partial class frmGestonarGrupo : System.Web.UI.Page
    {
        #region 'variable'
        DataSet losdatos = new DataSet();
        #endregion

        #region 'objeto'
        CLASS_GrupoTrabajo objtGrupoTrabajo = new CLASS_GrupoTrabajo();
        #endregion


         
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                try
                {
                    mostrar();
                }
                catch
                {
                    lblresultado.Text = "no se pueden cargar los datos. verifique";

                }

            }
        }

        public void mostrar()
        {
            losdatos = objtGrupoTrabajo.MostrarProyecto();
            GridGestionGrupos.DataSource = losdatos;
            GridGestionGrupos.DataBind();


        }

          

        protected void Button2_Click(object sender, EventArgs e)
        {

        }

        protected void gDProyecto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int x = int.Parse(GridGestionGrupos.DataKeys[e.RowIndex].Values["tb_pro_usuario_id_usuario"].ToString());
            int z = int.Parse(GridGestionGrupos.DataKeys[e.RowIndex].Values["tb_pro_grupo_trabajo_id_grupo_trabajo"].ToString());
            if (objtGrupoTrabajo.EliminarUsuarioGrupoTrabajo(x, z))
            {
                lblresultado.Text = "se elimino";
                mostrar();

            }
            else
                lblresultado.Text = "no se puedo eliminar";
        }

     
      
        
    }
}