﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CTPI_PROYECTOS_SLN_Plantilla.Master" AutoEventWireup="true" CodeBehind="frmCrearProyecto.aspx.cs" Inherits="CTPI_PROYECTOS_SLN.frmCrearProyecto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style11
        {
            width: 100%;
        }
        .style12
        {
            height: 21px;
        }
        .style13
        {
            height: 21px;
            width: 652px;
            text-align: justify;
        }
        .style15
        {
            width: 652px;
            text-align: justify;
        }
        .style16
        {
            height: 21px;
            width: 200px;
            text-align: justify;
        }
        .style17
        {
            width: 200px;
            text-align: justify;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Proyecto" 
        Height="451px">
        <table class="style11">
            <tr>
                <td class="style12">
                </td>
                <td class="style16" ID="lblNombreProyecto">
                    Nombre</td>
                <td class="style13">
                    <asp:TextBox ID="txtNombreProyecto" runat="server" Width="452px"></asp:TextBox>
                </td>
                <td class="style12">
                </td>
            </tr>
            <tr>
                <td class="style12">
                </td>
                <td ID="lblDescripcionProyecto" class="style16">
                    Descripción</td>
                <td class="style13">
                    <asp:TextBox ID="txtDescripcionProyecto" runat="server" Width="450px"></asp:TextBox>
                </td>
                <td class="style12">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td ID="lblResumenProyecto" class="style17">
                    Resumen</td>
                <td class="style15">
                    <asp:TextBox ID="txtResumenProyecto" runat="server" Width="450px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td ID="lblObjetivosProyecto" class="style17">
                    Objetivos</td>
                <td class="style15">
                   
                    <asp:TextBox ID="txtObjetivosProyecto" runat="server" Width="450px"></asp:TextBox>
                   
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style12">
                </td>
                <td ID="lblAreaProyecto" class="style16">
                    Área</td>
                <td class="style13">
                    <asp:TextBox ID="txtAreaProyecto" runat="server" Width="451px"></asp:TextBox>
                </td>
                <td class="style12">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td ID="lblFechaProyecto" class="style17">
                    Fecha</td>
                <td class="style15">
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" 
                        BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" 
                        ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px">
                        <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                        <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" 
                            VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                        <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" 
                            Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                        <TodayDayStyle BackColor="#CCCCCC" />
                    </asp:Calendar>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style12">
                    </td>
                <td class="style16" ID="lblGrupoProyecto">
                    Grupo</td>
                <td class="style13">
                    <asp:LinkButton ID="btnlnkGrupoProyecto" runat="server" 
                        onclick="lnkbtnGrupoProyecto_Click">Gestionar Grupo de Trabajo</asp:LinkButton>
                </td>
                <td class="style12">
                    </td>
            </tr>
            <tr>
                <td class="style12">
                    </td>
                <td ID="lblGrupoProyecto" class="style16">
                    </td>
                <td class="style13">
                    </td>
                <td class="style12">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td ID="lblGrupoProyecto" class="style17">
                    &nbsp;</td>
                <td class="style15">
                    <asp:Button ID="btnGuardarProyecto" runat="server" 
                        onclick="btnGuardarProyecto_Click" Text="Guardar" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    </asp:Content>
