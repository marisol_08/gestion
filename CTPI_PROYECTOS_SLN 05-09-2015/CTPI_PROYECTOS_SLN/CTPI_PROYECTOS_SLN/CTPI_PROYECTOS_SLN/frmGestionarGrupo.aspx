﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CTPI_PROYECTOS_SLN_Plantilla.Master" AutoEventWireup="true" CodeBehind="frmGestionarGrupo.aspx.cs" Inherits="CTPI_PROYECTOS_SLN.frmGestonarGrupo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
            width: 500px;
        }
        .style3
        {
            width: 2678px;
        }
        .style4
        {
            width: 375px;
        }
        .style5
        {
            margin: 0;
            padding: 0;
            list-style: none;
            width: 2678px;
        }
        .style6
        {
            width: 2182px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table >
        <tr>
            <td class="style6">
                &nbsp;</td>
            <td class="style2">
                <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txtNombre" runat="server" Width="200px" BackColor="#999999"></asp:TextBox>
            </td>
            <td class="style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style6">
                &nbsp;</td>
            <td class="style2">
                <asp:Label ID="lblDescripcion" runat="server" Text="Descripción"></asp:Label>
            </td>
            <td class="style3">
                <textarea id="txtArea" name="S1" rows="2"></textarea></td>
            <td class="style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style6">
                &nbsp;</td>
            <td class="style2">
                <asp:Label ID="lblGrupoDeTrabajo" runat="server" Text="Grupo de Trabajo"></asp:Label>
            </td>
            <td class="style5">
            <asp:Panel ID="Panel1" runat="server" BackColor="#D8C4A5" 
        GroupingText="Gestionar grupo de Trabajo">
                <asp:GridView ID="GridGestionGrupos" runat="server" Height="179px" 
                    Width="736px" AutoGenerateColumns="False" 
                    onselectedindexchanged="Page_Load" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                    DataKeyNames="tb_pro_usuario_id_usuario,tb_pro_usuario_nombres,tb_acc_programa_de_formacion_descripcion,tb_acc_roles_nombre,tb_acc_perfiles_nombre" 
                    onrowdeleting="gDProyecto_RowDeleting">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="tb_pro_usuario_id_usuario" FooterText="IdPerfil" 
                            HeaderText="IdPerfil" Visible="False" />
                        <asp:BoundField DataField="tb_pro_usuario_nombres" FooterText="Nombre" 
                            HeaderText="Nombre" />
                        <asp:BoundField DataField="tb_acc_programa_de_formacion_descripcion" 
                            FooterText="Programa de Formacion" HeaderText="Programa de Formacion" />
                        <asp:BoundField DataField="tb_acc_roles_nombre" FooterText="Rol" 
                            HeaderText="Rol" />
                        <asp:BoundField DataField="tb_acc_perfiles_nombre" FooterText="Perfil" 
                            HeaderText="Perfil" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                </asp:Panel>
            </td>
            <td class="style4">
                <asp:Button ID="btnAgregarMiembro" runat="server" Text="Agregar Miembro" 
                    Width="237px" />
            </td>
        </tr>
        <tr>
            <td class="style6">
                </td>
            <td class="style2" >
                </td>
            <td class="style3">
                <asp:Label ID="lblresultado" runat="server"></asp:Label>
                </td>
            <td ">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
