﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CTPI_PROYECTOS_SLN_Plantilla.Master" AutoEventWireup="true" CodeBehind="frmProyecto.aspx.cs" Inherits="CTPI_PROYECTOS_SLN.frmProyecto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table">
        <asp:Panel ID="Panel1" runat="server" GroupingText="Datos del Proyecto" 
        Width="837px">
            GESTIÓN DE PROYECTOS</td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td class="style4">
                    <asp:TextBox ID="txtBuscarProyecto" runat="server"></asp:TextBox>
                    <asp:Button ID="btnBuscarProyecto" runat="server" 
                    onclick="btnBuscarProyecto_Click" Text="BUSCAR" 
    Width="70px" />
                    <asp:Button ID="btnAgregarProyecto" runat="server" 
                    onclick="btnAgregarProyecto_Click" Text="Crear Proyecto" />
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:GridView ID="GriddatosProyecto" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" 
                        DataKeyNames="tb_pro_proyectos_id_proyecto,tb_pro_proyectos_nombreproyecto,tb_pro_proyectos_resumen,tb_pro_proyectos_descripcion,tb_pro_proyectos_objetivo,tb_pro_proyectos_area,tb_pro_proyectos_fecha" 
                        ForeColor="#333333" GridLines="None" 
                        onrowcancelingedit="gDProyecto_RowCancelingEdit" 
                        onrowdeleting="gDProyecto_RowDeleting" onrowediting="gDProyecto_RowEditing" 
                        onrowupdating="gDProyecto_RowUpdating" onselectedindexchanged="Page_Load" 
                        Width="802px">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="tb_pro_proyectos_id_proyecto" FooterText="Id" 
                                HeaderText="Id" />
                            <asp:BoundField DataField="tb_pro_proyectos_nombreproyecto" FooterText="Nombre" 
                                HeaderText="Nombre" />
                            <asp:BoundField DataField="tb_pro_proyectos_resumen" FooterText="Resumen" 
                                HeaderText="Resumen" />
                            <asp:BoundField DataField="tb_pro_proyectos_descripcion" 
                                FooterText="Descripcion" HeaderText="Descripcion" />
                            <asp:BoundField DataField="tb_pro_proyectos_objetivo" FooterText="Objetivos" 
                                HeaderText="Objetivos" />
                            <asp:BoundField DataField="tb_pro_proyectos_area" FooterText="Área" 
                                HeaderText="Área" />
                            <asp:BoundField DataField="tb_pro_proyectos_fecha" FooterText="Fecha" 
                                HeaderText="Fecha" />
                            <asp:CommandField ShowEditButton="True" />
                            <asp:CommandField ShowDeleteButton="True" />
                        </Columns>
                        <EditRowStyle BackColor="#7C6F57" />
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#E3EAEB" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>
                    <asp:Label ID="lblresultado" runat="server"></asp:Label>
                    <asp:Label ID="lblinsertar" runat="server"></asp:Label>
                    <br />
    </asp:Panel>
</asp:Content>
