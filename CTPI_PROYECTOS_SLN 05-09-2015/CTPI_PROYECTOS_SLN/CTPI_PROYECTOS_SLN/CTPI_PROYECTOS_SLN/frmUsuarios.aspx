﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CTPI_PROYECTOS_SLN_Plantilla.Master" AutoEventWireup="true" CodeBehind="frmUsuarios.aspx.cs" Inherits="CTPI_PROYECTOS_SLN.frmUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 171px;
        }
        .style3
        {
            width: 201px;
        }
        .style4
        {
            width: 461px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos de Usuario" 
        Height="194px">
        <table class="style1">
            <tr>
                <td class="style4">
                    &nbsp;</td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" Text="Id perfil"></asp:Label>
                    <asp:TextBox ID="TextId" runat="server" BackColor="Silver" Width="129px"></asp:TextBox>
                    <asp:Button ID="BtnBuscarporId" runat="server" onclick="BtnBuscarporId_Click" 
                        Text="Buscar por Id" />
                </td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="Label2" runat="server" Text="Descripcion"></asp:Label>
                    <asp:TextBox ID="TextDescripcion" runat="server" BackColor="Silver"></asp:TextBox>
                </td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    &nbsp;</td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="lblresultado" runat="server"></asp:Label>
                </td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Button ID="btnInsertar" runat="server" onclick="btnInsertar_Click" 
                        Text="Insertar" />
                </td>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
