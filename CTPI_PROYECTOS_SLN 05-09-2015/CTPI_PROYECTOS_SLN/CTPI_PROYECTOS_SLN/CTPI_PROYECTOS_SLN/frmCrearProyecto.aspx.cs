﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CTPI_PROYECTOS_SLN.Logica;


namespace CTPI_PROYECTOS_SLN
{
    public partial class frmCrearProyecto : System.Web.UI.Page
    {
        #region "Variables"
        DataSet losdatos = new DataSet();
        #endregion

        #region "objetos"
        CLASS_Proyectos objtproyecto = new CLASS_Proyectos();
        frmProyecto mensaje = new frmProyecto();

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardarProyecto_Click(object sender, EventArgs e)
        {
            DateTime fecha = new DateTime();
            fecha = Calendar1.SelectedDate;
          
            bool insertarProyecto;
            insertarProyecto = objtproyecto.InsertarProyecto(txtNombreProyecto.Text, txtDescripcionProyecto.Text, txtResumenProyecto.Text, txtAreaProyecto.Text, txtObjetivosProyecto.Text, Calendar1.SelectedDate);

            if (insertarProyecto==true)
            {
 
                Response.Redirect("frmProyecto.aspx");
                mensaje.mostrarMensaje();
                
                    
            }

        }

        protected void lnkbtnGrupoProyecto_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmGestionarGrupo.aspx");
        }

        
      
    }
}