﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using CTPI_PROYECTOS_SLN.localhost;


namespace CTPI_PROYECTOS_SLN.Logica
{
    public class CLASS_Proyectos
    {


        #region "atributos"
        private int id_proyecto;
        private int id_grupo_trabajo;

        private string descripcion;
        private string resumen;
        private string area;
        private string objetivo;
        private DateTime fecha;

        #endregion

        #region "metodos"

        public CLASS_Proyectos()
        {
            id_proyecto = 0;
            id_grupo_trabajo = 1;
            descripcion = "";
            resumen = "";
            area = "";
            objetivo = "";

        }
        public int Id_proyecto
        {
            set { value = id_proyecto; }
            get { return id_proyecto; }
        }

        public int Id_grupo_trabajo
        {
            set { value = id_grupo_trabajo; }
            get { return id_grupo_trabajo; }
        }

        public string Descripcion
        {
            set { value = descripcion; }
            get { return descripcion; }
        }
        public string Resumen
        {
            set { value = resumen; }
            get { return resumen; }
        }

        public string Area
        {
            set { value = area; }
            get { return area; }
        }

        public string Objetivo
        {
            set { value = objetivo; }
            get { return objetivo; }
        }

        public DateTime Fecha
        {
            set { value = fecha; }
            get { return fecha; }
        }

        #endregion

        #region "objetos:
        CTPI_PROYECTOS_WebService objCTPI_PROYECTOS_WebService = new CTPI_PROYECTOS_WebService();
        #endregion

        #region "Metodos para operaciones Crud"


        public bool InsertarProyecto(string nombre, string descripcion, string resumen, string area, string objetivo, DateTime fecha)
        {
            string fecha2 = fecha.ToUniversalTime().ToString("u");

            /* convert('" + fecha + "',date) */
            string strinsert = "INSERT INTO tb_pro_proyectos values (" + "''" + ",'" + nombre + "'," + id_grupo_trabajo + ",'" + fecha2 + "','" + descripcion + "','" + resumen + "','" + area + "','" + objetivo + "')";
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }

        public DataSet MostrarProyecto()
        {
            string cadena = "select * from tb_pro_proyectos";
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }


        public DataSet MostrarIdProyecto(int id_proyecto)
        {
            string cadena = "select tb_pro_proyectos_id_proyecto,tb_pro_proyectos_nombreproyecto,tb_pro_proyectos_resumen,tb_pro_proyectos_descripcion,tb_pro_proyectos_objetivo,tb_pro_proyectos_area,tb_pro_proyectos_fecha from tb_pro_proyectos where tb_pro_proyectos_id_proyecto = " + id_proyecto;
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }

        public DataSet MostrarDescripcionProyecto(string descripcion)
        {
            string cadena = "select tb_pro_proyectos_id_proyecto,tb_pro_proyectos_descripcion from tb_pro_proyectos where tb_pro_proyectos_descripcion LIKE '%" + descripcion + "%'";
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }


        public bool EliminarProyecto(int id_proyecto)
        {
            string strinsert = "DELETE FROM tb_pro_proyectos WHERE  tb_pro_proyectos_id_proyecto= " + id_proyecto;
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }


        public bool ActualizarProyecto(int id_proyecto, string nombre, string descripcion, string resumen, string objetivo, string area, string fecha)
        {
            string strinsert = "UPDATE tb_pro_proyectos SET tb_pro_proyectos_nombreproyecto= '" + nombre + "', tb_pro_proyectos_descripcion= '" + descripcion + "', tb_pro_proyectos_resumen = '" + resumen + "',  tb_pro_proyectos_objetivo = '" + objetivo + "', tb_pro_proyectos_area = '" + area + "', tb_pro_proyectos_fecha = '" + fecha + "'  WHERE tb_pro_proyectos_id_proyecto=" + id_proyecto;
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }



        #endregion
    }
}