﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using CTPI_PROYECTOS_SLN.localhost;

namespace CTPI_PROYECTOS_SLN.Logica
{
    public class CLASS_GrupoTrabajo
    {
        private string   nombre = "";
        private string   descripcion = "";
        private int id_GrupoTrabajo ;
        private int id_UsuarioGrupo;
        
        #region Constructor

        public CLASS_GrupoTrabajo()
        {
          nombre = "";
          descripcion = "";
          id_GrupoTrabajo = 0;
          id_UsuarioGrupo = 0;

        }

       
        #endregion
        #region Gets y Sets
        public string Nombre
        {
            set { value = nombre; }
            get { return nombre; }
        }

        public string Descripcion
        {
            set { value = descripcion; }
            get { return descripcion; }
        }

        public int Id_GrupoTrabajo
        {
            set { value = id_GrupoTrabajo; }
            get { return id_GrupoTrabajo; }
        }

        public int Id_UsuarioGrupo
        {
            set { value = id_UsuarioGrupo; }
            get { return id_UsuarioGrupo; }
        }
        
        #endregion

        #region "objetos"
        CTPI_PROYECTOS_WebService objCTPI_PROYECTOS_WebService = new CTPI_PROYECTOS_WebService();
        #endregion

        #region Metodos para crud


        public DataSet MostrarProyecto()
        {
            string cadena = "select tb_acc_usuario_grupo.tb_pro_usuario_id_usuario,tb_pro_usuario.tb_pro_usuario_nombres,tb_acc_programa_de_formacion.tb_acc_programa_de_formacion_descripcion,tb_acc_roles.tb_acc_roles_nombre,tb_acc_perfiles.tb_acc_perfiles_nombre from tb_pro_grupo_trabajo inner join tb_acc_usuario_grupo  on( tb_pro_grupo_trabajo.tb_pro_grupo_trabajo_id_grupo_trabajo = tb_acc_usuario_grupo.tb_acc_usuario_grupo_id_usuario_grupo) inner join tb_pro_usuario on (tb_acc_usuario_grupo.tb_pro_usuario_id_usuario = tb_pro_usuario.tb_pro_usuario_id_usuario) inner join tb_acc_programa_de_formacion on(tb_pro_usuario.tb_acc_programa_de_formacion_id_programa_de_formacion = tb_acc_programa_de_formacion.tb_acc_programa_de_formacion_id_programa_de_formaciion) inner join tb_acc_usuario_rol on (tb_pro_usuario.tb_pro_usuario_id_usuario = tb_acc_usuario_rol.tb_pro_usuario_id_usuario) inner join tb_acc_roles on(tb_acc_usuario_rol.tb_acc_roles_id_roles = tb_acc_roles.tb_acc_roles_id_roles) inner join tb_acc_perfiles on(tb_pro_usuario.tb_acc_perfiles_id_perfiles = tb_acc_perfiles.tb_acc_perfiles_id_perfiles)";
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }




        public bool EliminarUsuarioGrupoTrabajo(int id_GrupoTrabajo, int id_UsuarioGrupo)
        {
            string strinsert = "DELETE FROM tb_acc_usuario_grupo WHERE  tb_pro_usuario_id_usuario = " + id_UsuarioGrupo + "and tb_pro_grupo_trabajo_id_grupo_trabajo =" + id_GrupoTrabajo;
           return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }


        #endregion
    }
}