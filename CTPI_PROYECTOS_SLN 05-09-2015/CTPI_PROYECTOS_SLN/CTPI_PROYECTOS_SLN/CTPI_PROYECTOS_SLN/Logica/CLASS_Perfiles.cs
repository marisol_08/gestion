﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CTPI_PROYECTOS_SLN.localhost;
using System.Data;
namespace CTPI_PROYECTOS_SLN.logica
{
    public class CLASS_Perfiles
    {

        #region "atribuos"

        private int id;
        private string descipcion;
       

        #endregion
        #region "metodos"

        public CLASS_Perfiles() {
            id = 0;
            descipcion = "";
        
        }
        public int Id
        {
            set { value = id; }
            get { return id; }
        }


        public string Descripcion
        {
            set { value = descipcion; }
            get { return descipcion; }
        }
        


        #region "Metodos para operaciones Crud"
        CTPI_PROYECTOS_WebService objServicioBD = new CTPI_PROYECTOS_WebService();

        public bool InsertarPerfil(int proycodigo, string descripcion)
        {
            string strinsert = "INSERT INTO PERFILES values (" + proycodigo + ",'" + descripcion + "')";
            return objServicioBD.ejecutarDML(strinsert);
        }

        public DataSet MostrarPerfil()
        {
            string cadena = "select * from PERFILES";
            return objServicioBD.EjecutarConsulta(cadena);
        }


        public DataSet MostrarPerfilCodigo(int idperfil)
        {
            string cadena = "select descripcion from PERFILES where ID_PERFIL = " + idperfil;
            return objServicioBD.EjecutarConsulta(cadena);
        }

        public DataSet MostrarPerfilCodigo(string descripcion)
        {
            string cadena = "select id_perfil,descripcion from PERFILES where descripcion LIKE '%" + descripcion + "%'";
            return objServicioBD.EjecutarConsulta(cadena);
        }


        public bool EliminarPerfil(int idperfil)
        {
            string strinsert = "DELETE FROM PERFILES WHERE ID_PERFIL = " + idperfil;
            return objServicioBD.ejecutarDML(strinsert);
        }


        public bool ActualizarPerfil(int idperfil, string descripcion)
        {
            string strinsert = "UPDATE PERFILES SET descripcion= '" + descripcion + "' WHERE ID_PERFIL =" + idperfil;
            return objServicioBD.ejecutarDML(strinsert);
        }

        #endregion


       


        #endregion

        #region "objetos:"
        /* CTPI_PROYECTOS_WebService objServicioBD = new CTPI_PROYECTOS_WebService();
        #endregion

        #region "Metodos para operaciones Crud"


        public bool InsertarPerfil(int proycodigo, string descripcion)
        {
            string strinsert = "INSERT INTO PERFILES values (" + proycodigo + ",'" + descripcion + "')";
            return objServicioBD.ejecutarDML(strinsert);
        }

        public DataSet MostrarPerfil()
        {
            string cadena = "select * from PERFILES";
            return objServicioBD.EjecutarConsulta(cadena);
        }


        public DataSet MostrarPerfilCodigo(int idperfil)
        {
            string cadena = "select descripcion from PERFILES where ID_PERFIL = "+idperfil;
            return objServicioBD.EjecutarConsulta(cadena);
        }

        public DataSet MostrarPerfilCodigo(string descripcion)
        {
            string cadena = "select id_perfil,descripcion from PERFILES where descripcion LIKE '%" + descripcion + "%'";
            return objServicioBD.EjecutarConsulta(cadena);
        }


        public bool EliminarPerfil(int idperfil)
        {
            string strinsert = "DELETE FROM PERFILES WHERE ID_PERFIL = " + idperfil;
            return objServicioBD.ejecutarDML(strinsert);
        }


        public bool ActualizarPerfil(int idperfil,string descripcion)
        {
            string strinsert = "UPDATE PERFILES SET descripcion= '"+descripcion+"' WHERE ID_PERFIL =" + idperfil;
            return objServicioBD.ejecutarDML(strinsert);
        }
        */
        #endregion
    }
}