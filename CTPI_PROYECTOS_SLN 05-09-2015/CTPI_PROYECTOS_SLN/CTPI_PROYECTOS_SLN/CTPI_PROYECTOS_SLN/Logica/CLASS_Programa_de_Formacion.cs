﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CTPI_PROYECTOS_SLN.localhost;
using System.Data;

namespace CTPI_PROYECTOS_SLN.Logica
{
    public class CLASS_Programa_de_Formacion
    {

        #region atributos
        private int id;
        private string nombre;
        private string descripcion;

        #endregion
        #region Constructor

        public CLASS_Programa_de_Formacion()
        {
            id = 0;
            descripcion = "";
            nombre = "";
        }
        #endregion

        #region Gets y Sets
        public int idProgramaFormacion
        {
            set { value = id; }
            get { return id; }
        }

        public string nombreProgramaFormacion
        {
            set { value = nombre; }
            get { return nombre; }
        }

        public string descripcionProgramaFormacion
        {
            set { value = descripcion; }
            get { return descripcion; }
        }       
        #endregion

        #region Metodos

        CTPI_PROYECTOS_WebService objCTPI_PROYECTOS_WebService = new CTPI_PROYECTOS_WebService();

        public bool InsertarProgramaFormacion(int id, string nombre, string descripcion)
        {
            string strinsert = "INSERT INTO tb_acc_programa_de_formacion values (" + id +",'"+ nombre + "','"+ descripcion + "')";
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }

        public DataSet MostrarProgramaFormacion()
        {
            string cadena = "select * from tb_acc_programa_de_formacion";
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }


        public DataSet MostrarIdProgramaFormacion(int id_programa)
        {
            string cadena = "select tb_acc_programa_de_formacion_nombre, tb_acc_programa_de_formacion_descripcion from tb_acc_programa_de_formacion where tb_acc_programa_de_formacion_id_programa_de_formacion = " + id_programa;
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }

        public DataSet MostrarNombreProgramaFormacion(string nombre)
        {
            string cadena = "select tb_acc_programa_de_formacion_id_programa_de_formacion, tb_acc_programa_de_formacion_descripcion from tb_acc_programa_de_formacion where tb_acc_programa_de_formacion_descripcion LIKE '%" + nombre + "%'";
            return objCTPI_PROYECTOS_WebService.EjecutarConsulta(cadena);
        }


        public bool EliminarProgramaFormacion(int id_programa)
        {
            string strinsert = "DELETE FROM tb_acc_programa_de_formacion WHERE  tb_acc_programa_de_formacion_id_programa_de_formacion= " + id_programa;
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }


        public bool ActualizarProgramaFormacion(int id_programa, string nombre, string descripcion)
        {
            string strinsert = "UPDATE tb_acc_programa_de_formacion SET tb_acc_programa_de_formacion_nombre= '" + nombre + ", tb_acc_programa_de_formacion_descripcion= '" + descripcion + "' WHERE ID_PROYECTO =" + id_programa;
            return objCTPI_PROYECTOS_WebService.ejecutarDML(strinsert);
        }
        #endregion
    }
}