﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CTPI_PROYECTOS_SLN_Plantilla.Master" AutoEventWireup="true" CodeBehind="frmRoles.aspx.cs" Inherits="CTPI_PROYECTOS_SLN.frmRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 403px;
        }
        .style3
        {
            height: 21px;
            width: 250px;
        }
        .style4
        {
            width: 403px;
            height: 21px;
        }
        .style5
        {
            width: 184px;
            height: 21px;
        }
        .style6
        {
            width: 175px;
        }
        .style7
        {
            width: 250px;
        }
        .style8
        {
            height: 21px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" Height="287px" BackColor="White" 
        GroupingText="Datos Proyecto">
        <table class="style1">
            <tr>
                <td class="style8">
                </td>
                <td class="style4">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                <td class="style3">
                </td>
            </tr>
            <tr>
                <td class="style6">
                    &nbsp;</td>
                <td class="style2">
                    <asp:GridView ID="GriddatosRol" runat="server" AutoGenerateColumns="False" 
                        Height="150px" 
                        Width="480px" DataKeyNames="tb_pro_usuario_nombres,tb_acc_roles_nombre" 
                        BorderColor="#6B91C9" BorderStyle="Solid" HorizontalAlign="Center" 
                        onrowcancelingedit="gDRoles_RowCancelingEdit" 
                        onrowdeleting="gDRoles_RowDeleting" onrowediting="gDRoles_RowEditing" 
                        onrowupdating="gDRoles_RowUpdating" onselectedindexchanged="Page_Load">
                        <Columns>
                            <asp:BoundField DataField="tb_pro_usuario_nombres" FooterText="Usuario" 
                                HeaderText="Usuario" >
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#49A0CB" Height="5px" HorizontalAlign="Center" />
                            <ItemStyle Height="10px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="tb_acc_roles_nombre" FooterText="Rol" 
                                HeaderText="Rol" >
                            <HeaderStyle BackColor="#49A0CB" Height="5px" HorizontalAlign="Center" />
                            <ItemStyle Height="10px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:CommandField ShowEditButton="True" >
                            <HeaderStyle BackColor="#49A0CB" Height="5px" HorizontalAlign="Center" />
                            <ItemStyle Height="10px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:CommandField>
                            <asp:CommandField ShowDeleteButton="True" >
                            <HeaderStyle BackColor="#49A0CB" Height="5px" HorizontalAlign="Center" />
                            <ItemStyle Height="10px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:CommandField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td class="style7">
                   </td>
            </tr>
            <tr>
                <td class="style8" bgcolor="#3393B3" colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style8">
                    Código</td>
                <td class="style4">
                    <asp:TextBox ID="txtCodigoRol" runat="server"></asp:TextBox>
                </td>
                <td class="style3">
                    <asp:Label ID="lblresultado" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    Descripción</td>
                <td class="style4">
                    <asp:TextBox ID="txtDescripcionRol" runat="server" ></asp:TextBox>
                </td>
                <td class="style3">
                    </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
