﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CTPI_PROYECTOS_SLN.Logica;

namespace CTPI_PROYECTOS_SLN
{
    public partial class frmRoles : System.Web.UI.Page
    {
        #region "Variables"
        DataSet losdatos = new DataSet();
        #endregion

        #region "objetos"
        CLASS_Roles objtrol = new CLASS_Roles();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    mostrar();
                }
                catch
                {
                    lblresultado.Text = "no se pueden cargar lo datos";
                }

            }
        }


        public void mostrar()
        {
            losdatos = objtrol.MostrarRol();
            GriddatosRol.DataSource = losdatos;
            GriddatosRol.DataBind();


        }

        protected void gDRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gDRoles_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GriddatosRol.EditIndex = Convert.ToInt16(e.NewEditIndex);
            HiddenField1.Value = e.NewEditIndex.ToString();
            mostrar();
        }

        protected void gDRoles_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
          //  TextBox Tid = new TextBox();
          //  TextBox TUsuario = new TextBox();
            //TextBox TRol = new TextBox();
            

            //recupera las cajas de texto de lagrilla
            //TUsuario = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[0].Controls[0];
            //TRol = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[1].Controls[0];
           
            //objtrol.ActualizarProyecto(TUsuario.Text, TRol.Text);

//            GriddatosRol.EditIndex = -1;
  //          mostrar();
        }

        protected void gDRoles_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //pemirte que no se haga ningun cambio
            GriddatosRol.EditIndex = -1;
            mostrar();

        }
        /*
        public void mostrarMensaje()
        {
            lblinsertar.Text = "Los resultados se cargaron correctamente";
        }

        protected void btnBuscarProyecto_Click(object sender, EventArgs e)
        {

            losdatos = objtrol.MostrarIdProyecto(int.Parse(txtBuscarProyecto.Text));
            GriddatosRol.DataSource = losdatos;
            GriddatosRol.DataBind();

        }

        protected void btnAgregarProyecto_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmCrearProyecto.aspx");
        }





        protected void gDProyecto_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //pemirte que no se haga ningun cambio
            GriddatosRol.EditIndex = -1;
            mostrar();
        }

        protected void gDProyecto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int x = int.Parse(GriddatosRol.DataKeys[e.RowIndex].Values["tb_pro_proyectos_id_proyecto"].ToString());
            if (objtrol.EliminarProyecto(x))
            {
                lblresultado.Text = "se eliminó";
                mostrar();

            }
            else
                lblresultado.Text = "no se puedo eliminar";
        }

        protected void gDProyecto_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //que permite que se active el link editar
            GriddatosRol.EditIndex = Convert.ToInt16(e.NewEditIndex);
            HiddenField1.Value = e.NewEditIndex.ToString();
            mostrar();
        }

        protected void gDProyecto_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TextBox Tid = new TextBox();
            TextBox TNombre = new TextBox();
            TextBox TDescripcion = new TextBox();
            TextBox TResumen = new TextBox();
            TextBox TObjetivo = new TextBox();
            TextBox TArea = new TextBox();
            TextBox TFecha = new TextBox();

            //recupera las cajas de texto de lagrilla
            Tid = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[0].Controls[0];
            TNombre = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[1].Controls[0];
            TDescripcion = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[2].Controls[0];
            TResumen = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[3].Controls[0];
            TObjetivo = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[4].Controls[0];
            TArea = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[5].Controls[0];
            TFecha = (TextBox)GriddatosRol.Rows[Convert.ToInt16(HiddenField1.Value)].Cells[6].Controls[0];

            objtrol.ActualizarProyecto(int.Parse(Tid.Text), TNombre.Text, TDescripcion.Text, TResumen.Text, TObjetivo.Text, TArea.Text, TFecha.Text);

            GriddatosRol.EditIndex = -1;
            mostrar();
        }

      
        */
     
    }
}