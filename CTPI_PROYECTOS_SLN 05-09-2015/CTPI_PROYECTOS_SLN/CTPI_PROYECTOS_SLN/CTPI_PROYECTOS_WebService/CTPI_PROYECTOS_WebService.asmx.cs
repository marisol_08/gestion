﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace CTPI_PROYECTOS_WebService
{
    /// <summary>
    /// Descripción breve de CTPI_PROYECTOS_WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class CTPI_PROYECTOS_WebService : System.Web.Services.WebService
    {
        MySqlConnection conexion = new MySqlConnection();

        #region "conexion"
        [WebMethod]
        public bool conectar()
        {
            try
            {
                conexion.ConnectionString = "Server=localhost;Database=pgp_db_proyecto_gestion_de_proyectos_database; Uid=root;Pwd=;";
                conexion.Open();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        #endregion

        #region "Metodo para ejecutar un select"
        [WebMethod]
        public DataSet EjecutarConsulta(string sql)
        {

            conectar();
            DataSet datos = new DataSet();
            MySqlDataAdapter miadaptador = new MySqlDataAdapter(sql, conexion);
            miadaptador.Fill(datos);
            return datos;
        }

        #endregion

        #region "metodo para ejecutar un insert, update ,delte"

        [WebMethod]
        public bool ejecutarDML(string DML)
        {
            if (conectar() == true)
            {
                MySqlCommand comando = new MySqlCommand(DML, conexion);
                if (comando.ExecuteNonQuery() > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        #endregion
    }
}
